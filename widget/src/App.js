import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import Post from './components/post';

class App extends Component {

	state = {
		posts: [],
		nextPage: '',
		status: '',
		type: ''
	}

  	componentDidMount = () => {
		axios.get('http://insided.javiernarvaez.com/api/posts/read.php?count=3').then((response) => {
			this.setState({ posts: response.data.data, nextPage: response.data.paging.pages[1].url });
		}).catch(e => {
			this.setState({ status: 'Comment submission failed', type: 'error' });
		});
	}
	  
	dismissStatus = () => {
		this.setState({ status: '', type: '' });
	}

	loadMore = () => {
		axios.get(this.state.nextPage).then((response) => {

			const newPosts = [...this.state.posts, ...response.data.data];
			const nextPage = response.data.paging.pages.findIndex(page => page.current_page === 'yes') + 1;

			this.setState({ posts: newPosts, nextPage: response.data.paging.pages[nextPage].url });

		}).catch(e => {
			this.setState({ status: 'Comment submission failed', type: 'error' });
		});
	}

  	render() {
		const statusMessage = this.state.status !== '' ? (
			<div className={'form-status ' + this.state.type}>
				{this.state.status}
				<div>
					<button className="dismiss" onClick={() => this.dismissStatus()}>Dismiss</button>
				</div>
			</div>
		) : null;

		const posts = (this.state.posts.length > 0) ? (
			<div>
			{
				this.state.posts.map((post, i) => {
					return <Post key={post.id} data={post} />
				})
			}
			</div>
		): null;

		return (
			<div className="widget">
				<div className="post-container">
					{posts}
				</div>
				<div className="load-more-container">
					<button className="load-more" onClick={() => this.loadMore()}>Load more</button>
				</div>
				{statusMessage}
			</div>
		);
  	}
}

export default App;
