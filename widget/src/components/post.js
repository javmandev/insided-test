import React from 'react';

const Post = (props) => {
    return (
        <div className="post">
            <div className="user">{props.data.userId} - <span>{props.data.timestamp}</span></div>
            <div>{props.data.message}</div>
        </div>
    );
}

export default Post;