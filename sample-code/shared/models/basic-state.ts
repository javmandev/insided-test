export interface BasicState {
	isLoading: boolean;
	errors: any[];
	hasErrors: boolean;
}
