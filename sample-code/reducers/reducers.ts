import { ActionReducerMap, ActionReducer, MetaReducer } from '@ngrx/store';

import * as fromRouter from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from '../../environments/environment';
import { RouterStateUrl } from '../shared/models/router-state';

export interface AppState {
	router: fromRouter.RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<AppState> = {
	router: fromRouter.routerReducer
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production
	? [storeFreeze]
	: [];
