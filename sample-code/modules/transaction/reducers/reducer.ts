import { ActionReducer, createFeatureSelector, createSelector } from '@ngrx/store';

import { BasicState } from '../../../shared/models/basic-state';

import * as actions from '../actions/actions';
import { Transaction } from '../models/transaction';

export interface State extends BasicState {
	transaction: Transaction[];
}

export const INITIAL_STATE: State = {
	transaction: [],
	isLoading: false,
	hasErrors: false,
	errors: []
};

export function reducer(state = INITIAL_STATE, action: actions.Actions): State {

	switch (action.type) {

		case actions.LOAD_TRANSACTION:
		case actions.LOAD_ALL_TRANSACTIONS:
		case actions.SAVE_TRANSACTION:
		case actions.EDIT_TRANSACTION:
			return handleAction(state);

		case actions.LOAD_TRANSACTION_SUCCESS:
		case actions.SAVE_TRANSACTION_SUCCESS:
			return handleSuccess(state, action);

		case actions.LOAD_ALL_TRANSACTIONS_SUCCESS:
			return handleLoadAllSuccess(state, action);

		case actions.EDIT_TRANSACTION_SUCCESS:
			return handleVoidSuccess(state, action);

		case actions.LOAD_TRANSACTION_FAIL:
		case actions.LOAD_ALL_TRANSACTIONS_FAIL:
		case actions.SAVE_TRANSACTION_FAIL:
		case actions.EDIT_TRANSACTION_FAIL:
			return handleFail(state, action);

		default:
			return state;

	}
}

function handleAction(state: State): State {
	return {
		...state,
		transaction: [ ...state.transaction ],
		isLoading: true
	};
}

function handleSuccess(state: State, action: actions.Actions): State {
	return {
		...state,
		transaction: [ ...action.payload, ...state.transaction ],
		isLoading: false,
		hasErrors: false
	};
}

function handleLoadAllSuccess(state: State, action: actions.Actions): State {
	return {
		...state,
		transaction: [ ...action.payload ],
		isLoading: false,
		hasErrors: false
	};
}

function handleVoidSuccess(state: State, action: actions.Actions): State {
	return {
		...state,
		transaction: [ ...state.transaction ],
		isLoading: false,
		hasErrors: false
	};
}

function handleFail(state: State, action: actions.Actions): State {
	return {
		...state,
		transaction: [ ...state.transaction ],
		errors: [ ...state.errors, action.payload ],
		isLoading: false,
		hasErrors: true
	};
}

export const getState = createFeatureSelector<State>('transaction');

export const getTransactions = createSelector(
	getState,
	state => state.transaction
);
