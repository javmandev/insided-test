import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { State, getTransactions } from '../reducers/reducer';
import { Transaction } from '../models/transaction';
import { LoadAll } from '../actions/actions';

@Injectable()
export class TransactionsLoadedResolver implements Resolve<Transaction[]> {

 	constructor(
		private store: Store<State>
	) {}

  	resolve(route: ActivatedRouteSnapshot): Observable<Transaction[]> {
		this.store.dispatch(new LoadAll);
		return;
	}
}
