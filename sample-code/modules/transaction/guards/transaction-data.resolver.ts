import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { TransactionService } from '../providers/transaction';
import { Transaction } from '../models/transaction';

@Injectable()
export class TransactionDataResolver implements Resolve<Transaction> {

	constructor(
		private transactionService: TransactionService
	) {}

  	resolve(route: ActivatedRouteSnapshot): Observable<Transaction> {

		return this.transactionService.getTransactionById(+route.paramMap.get('id'));
	}
}
