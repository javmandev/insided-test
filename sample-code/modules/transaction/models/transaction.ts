export interface Transaction {
	id: number;
	amount: number;
	category: number;
	description: string;
	timestamp: Date;
	income: boolean;
	account_id: number;
}
