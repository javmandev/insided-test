import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';

import { Transaction } from '../models/transaction';

@Injectable()
export class TransactionService {

	private api: string = 'http://localhost:3000/transactions';

	constructor(private http: HttpClient) {}

	saveTransaction(transaction: Transaction): Observable<any> {

		return this.http.post(`${this.api}/new`, transaction)
			.flatMap(() => this.http.get(`${this.api}/last`));
	}

	editTransaction(transaction: Transaction): Observable<any> {

		return this.http.put(`${this.api}/edit`, transaction);
	}

	loadAllTransactions(): Observable<any> {

		return this.http.get(`${this.api}/`);
	}

	getTransactionsByMonth(year?, month?): Observable<any> {

		let params = '';

		if (year && month) {
			params = `${year}/${month}`;
		}

		return this.http.get(`${this.api}/bymonth/${params}`);
	}

	getTransactionById(id): Observable<any> {

		return this.http.get(`${this.api}/${id}`);
	}

	processTransactionsByDay(transactions) {

		const tbd = [];

		transactions.map((t, index) => {

			const currDate = new Date(t.timestamp);
			const currDateFormatted = `${currDate.getMonth() + 1}/${currDate.getDate()}/${currDate.getFullYear()}`;
			let prevDate, prevDateFormatted;

			if ( transactions[index - 1] ) {
				prevDate = new Date(transactions[index - 1].timestamp);
				prevDateFormatted = `${prevDate.getMonth() + 1}/${prevDate.getDate()}/${prevDate.getFullYear()}`;

				if (prevDateFormatted === currDateFormatted) {
					const last = tbd[tbd.length - 1];
						last.transactions.push(t);

					if (t.income) {
						last.total += t.amount;
						last.income += t.amount;
					} else {
						last.total -= t.amount;
						last.outcome += t.amount;
					}
				}
			}
			if ( !transactions[index - 1] || prevDateFormatted !== currDateFormatted) {
				tbd.push({
					date: currDateFormatted,
					total: (t.income) ? t.amount : -t.amount,
					income: (t.income) ? t.amount : 0,
					outcome: (t.income) ? 0 : t.amount,
					transactions: [ t ]
				});
			}
		});

		return tbd;
	}
}
