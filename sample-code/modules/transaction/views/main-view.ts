import { Component } from '@angular/core';

@Component({
	selector: 'main-view',
	template:
			`
                <new-transaction></new-transaction>
                <view-transactions><view-transactions>
            `
})
export class TransactionMainViewComponent {}
