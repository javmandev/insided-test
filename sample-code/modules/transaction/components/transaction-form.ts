import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Category } from '../../category/models/category';

@Component({
	selector: 'transaction-form-component',
	templateUrl: './transaction-form.html',
	styleUrls: ['./transaction-form.scss']
})
export class TransactionFormComponent {

	public months: string[] = [
		'January', 'February', 'March', 'April',
		'May', 'June', 'July', 'August',
		'September', 'October', 'November', 'December'];

	@Input() form: FormGroup;
	@Input() showCategoryModal: boolean = false;

	@Output() onSubmitForm = new EventEmitter;

	public get editing(): boolean {
		return this.form.value['id'] ? true : false;
	}

	public get years(): number[] {

		const year: number = new Date().getFullYear();
		const years: number[] = [];

		for (let i = 0; i < 10; i++) {
			years.push(year - i);
		}

		return years;
	}

	public get days(): number[] {

		const days: number[] = [];
		const totalDays: number = new Date(this.form.value.year, this.form.value.month + 1, 0).getDate();

		for (let i = 1; i <= totalDays; i++) {
			days.push(i);
		}

		return days;
	}

	submitForm() {
		this.onSubmitForm.emit();
	}
}
