import { Component, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { Transaction } from '../models/transaction';
import { Category } from '../../category/models/category';

@Component({
	selector: 'view-transactions-component',
	templateUrl: './view-transactions.html',
	styleUrls: ['./view-transactions.scss'],
	animations: [
		trigger('show', [
			state(':enter, true', style({ height: '*' })),
			state('void, false', style({ height: '0px' })),
			transition('* => *', animate('300ms ease-out'))
		])
	]
})
export class ViewTransactionsComponent {

	public showDays: boolean[] = [];

	@Input() transactions: Transaction[];
	@Input() categories: Category[];

	getCategoryIcon(id: number): any {
		return this.categories.filter(c => c.id === id).map(c => c.icon);
	}

	trackTransactions(index: number, transaction: Transaction) {
		return transaction ? transaction.id : undefined;
	}

	trackTransactionsByDay(index: number, transactionByDay: any) {
		return transactionByDay ? transactionByDay.date : undefined;
	}

	toggleDays(id: number): void {
		this.showDays[id] = !this.showDays[id];
	}
}
