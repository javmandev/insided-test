import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { State } from '../reducers/reducer';
import * as fromCategory from '../../category/reducers/reducer';

import { Save } from '../actions/actions';
import { Category } from '../../category/models/category';
import { CategoryService } from '../../category/providers/category';

@Component({
	selector: 'new-transaction',
	template: `<transaction-form-component
				[form]="transactionForm"
				(onSubmitForm)="handleSubmitForm($event)">
			</transaction-form-component>`
})
export class NewTransactionContainer {

	public transactionForm: FormGroup;
	private defCategory: Category;
	private selCategory: Category;

	constructor(
		private store: Store<State>,
		private categoryStore: Store<fromCategory.State>,
		private categoryService: CategoryService,
		private fb: FormBuilder
	) {
		this.selectedCategory.subscribe(c => {
			if (c) {
				this.selCategory = { ...c };
			}
		});

		this.setupForm();
	}

	private get selectedCategory() {
		return this.categoryStore.select(fromCategory.getSelectedCategory);
	}

	setupForm() {
		this.transactionForm = this.fb.group({
			amount: [ undefined, [ Validators.required, Validators.min(0) ] ],
			category: this.selCategory.id,
			description: '',
			account_id: 1,
			income: 0
		});
	}

	handleSubmitForm(e) {
		this.transactionForm.value.category = this.selCategory.id;
		this.store.dispatch(new Save(this.transactionForm.value));
		this.setupForm();
	}
}
