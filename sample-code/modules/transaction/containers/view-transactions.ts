import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { State, getTransactions } from '../reducers/reducer';
import { LoadAll } from '../actions/actions';
import { getCategories, State as CategoryState } from '../../category/reducers/reducer';

import { TransactionService } from '../providers/transaction';

@Component({
	selector: 'view-transactions',
	template: `<view-transactions-component
				[transactions]="transactionsByDay"
				[categories]="categories | async">
			</view-transactions-component>`
})
export class ViewTransactionsContainer {

	public transactionsByDay;

	constructor(
		private store: Store<State>,
		private categoryStore: Store<CategoryState>,
		private transactionService: TransactionService
	) {
		this.transactions.subscribe(t => this.transactionsByDay = this.transactionService.processTransactionsByDay(t));
	}

	public get transactions() {
		return this.store.select(getTransactions);
	}

	public get categories() {
		return this.categoryStore.select(getCategories);
	}
}
