import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { State } from '../reducers/reducer';
import * as fromCategory from '../../category/reducers/reducer';

import { Edit } from '../actions/actions';
import { Select } from '../../category/actions/actions';

import { CategoryService } from '../../category/providers/category';

import { Category } from '../../category/models/category';
import { Transaction } from '../models/transaction';

@Component({
	selector: 'edit-transaction',
	template: `<transaction-form-component
				[form]="transactionForm"
				(onSubmitForm)="handleSubmitForm($event)">
			</transaction-form-component>`
})
export class EditTransactionContainer {

	public transaction: Transaction;
	public transactionForm: FormGroup;
	private selCategory: Category;

	constructor(
		private store: Store<State>,
		private categoryStore: Store<fromCategory.State>,
		private categoryService: CategoryService,
		private activatedRoute: ActivatedRoute,
		private fb: FormBuilder
	) {

		this.transaction = this.activatedRoute.snapshot.data.transaction;
		this.categoryStore.dispatch(
			new Select(
				this.categoryService.findCategoryById(this.transaction.category)
			)
		);

		this.selectedCategory.subscribe(c => {
			if (c) {
				this.selCategory = { ...c };
			}
		});

		this.setupForm();
	}

	private get selectedCategory() {
		return this.categoryStore.select(fromCategory.getSelectedCategory);
	}

	setupForm() {
		const date = new Date(this.transaction.timestamp);

		this.transactionForm = this.fb.group({
			id: this.transaction.id,
			amount: [ this.transaction.amount, [ Validators.required, Validators.min(0) ] ],
			category: this.transaction.category,
			description: this.transaction.description,
			account_id: this.transaction.account_id,
			income: this.transaction.income,
			year: date.getFullYear(),
			month: date.getMonth(),
			day: date.getDate(),
			timestamp: this.transaction.timestamp
		});
	}

	handleSubmitForm(e) {
		this.transactionForm.value.category = this.selCategory.id;
		this.transactionForm.value.timestamp =
			new Date(`${this.transactionForm.value.month + 1}/${this.transactionForm.value.day}/${this.transactionForm.value.year}`);
		this.store.dispatch(new Edit(this.transactionForm.value));
	}

}
