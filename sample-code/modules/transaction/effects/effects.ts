import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';

import * as actions from '../actions/actions';
import * as categoryActions from '../../category/actions/actions';
import { Transaction } from '../models/transaction';
import { TransactionService } from '../providers/transaction';

@Injectable()
export class TransactionEffects {
	constructor (
		private actions$: Actions,
		private router: Router,
		private transactionService: TransactionService
	) {}

	@Effect() saveTransaction$: Observable<Action> = this.actions$
		.ofType(actions.SAVE_TRANSACTION)
		.map<Action, Transaction>(toPayload)
		.switchMap(transaction => this.transactionService.saveTransaction(transaction)
			.mergeMap(newTransaction => {
				return [
					new actions.SaveSuccess(newTransaction),
					new categoryActions.SelectDefault()
				];
			})
			.catch(error => Observable.of(new actions.SaveFail(error)))
		);

	@Effect() editTransaction$: Observable<Action> = this.actions$
		.ofType(actions.EDIT_TRANSACTION)
		.map<Action, Transaction>(toPayload)
		.switchMap(transaction => this.transactionService.editTransaction(transaction)
			.mergeMap(editedTransaction => {
				this.router.navigate(['transactions']);
				return [
					new actions.EditSuccess(editedTransaction),
					new categoryActions.SelectDefault()
				];
			})
			.catch(error => Observable.of(new actions.EditFail(error)))
		);

	@Effect() loadAllTransactions$: Observable<Action> = this.actions$
		.ofType(actions.LOAD_ALL_TRANSACTIONS)
		.switchMap(() => this.transactionService.getTransactionsByMonth()
			.map(allTransactions => new actions.LoadAllSuccess(allTransactions))
			.catch(error => Observable.of(new actions.LoadAllFail(error)))
		);
}
