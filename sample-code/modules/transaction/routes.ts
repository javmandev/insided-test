import { Routes } from '@angular/router';
import { TransactionMainViewComponent } from './views/main-view';
import { EditTransactionContainer } from './containers/edit-transaction';
import { CategoriesLoadedGuard } from '../category/guards/categories-loaded.guard';
import { TransactionDataResolver } from './guards/transaction-data.resolver';
import { TransactionsLoadedResolver } from './guards/transactions-loaded.resolver';

export const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'transactions'
	},
	{
		path: 'transactions',
		canActivate: [ CategoriesLoadedGuard ],
		children: [
			{
				path: '',
				pathMatch: 'full',
				component: TransactionMainViewComponent,
				resolve: { allTransactions: TransactionsLoadedResolver }
			},
			{
				path: 'edit',
				children: [
					{
						path: '',
						pathMatch: 'full',
						redirectTo: '/transactions'
					},
					{
						path: ':id',
						component: EditTransactionContainer,
						resolve: { transaction: TransactionDataResolver }
					}
				]
			}
		]
	}
];
