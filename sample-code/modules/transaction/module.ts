// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { CategoryModule } from '../category/module';

// Containers / Components
import { TransactionMainViewComponent } from './views/main-view';
import { NewTransactionContainer } from './containers/new-transaction';
import { EditTransactionContainer } from './containers/edit-transaction';
import { TransactionFormComponent } from './components/transaction-form';
import { ViewTransactionsContainer } from './containers/view-transactions';
import { ViewTransactionsComponent } from './components/view-transactions';

// Providers
import { TransactionService } from './providers/transaction';
import { TransactionDataResolver } from './guards/transaction-data.resolver';
import { TransactionsLoadedResolver } from './guards/transactions-loaded.resolver';

// Reducers
import { reducer } from './reducers/reducer';

// Effects
import { TransactionEffects } from './effects/effects';

// Routes
import { routes } from './routes';

const COMPONENTS = [
	TransactionMainViewComponent,
	NewTransactionContainer,
	EditTransactionContainer,
	TransactionFormComponent,
	ViewTransactionsContainer,
	ViewTransactionsComponent
];

@NgModule({
	declarations: COMPONENTS,
	providers: [
		TransactionService,
		TransactionDataResolver,
		TransactionsLoadedResolver
	],
	imports: [
		CommonModule,
		ReactiveFormsModule,
		HttpClientModule,
		StoreModule.forFeature('transaction', reducer),
		EffectsModule.forFeature([TransactionEffects]),
		RouterModule.forChild(routes),
		CategoryModule
	],
	exports: COMPONENTS
})
export class TransactionModule {}
