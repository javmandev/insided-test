import { Action } from '@ngrx/store';

export const LOAD_TRANSACTION = '[Transaction] Load';
export const LOAD_TRANSACTION_SUCCESS = '[Transaction] Load Success';
export const LOAD_TRANSACTION_FAIL = '[Transaction] Load Fail';

export const LOAD_ALL_TRANSACTIONS = '[Transaction] Load All';
export const LOAD_ALL_TRANSACTIONS_SUCCESS = '[Transaction] Load All Success';
export const LOAD_ALL_TRANSACTIONS_FAIL = '[Transaction] Load All Fail';

export const SAVE_TRANSACTION = '[Transaction] Save';
export const SAVE_TRANSACTION_SUCCESS = '[Transaction] Save Success';
export const SAVE_TRANSACTION_FAIL = '[Transaction] Save Fail';

export const EDIT_TRANSACTION = '[Transaction] Edit';
export const EDIT_TRANSACTION_SUCCESS = '[Transaction] Edit Success';
export const EDIT_TRANSACTION_FAIL = '[Transaction] Edit Fail';

export class Load implements Action {
	readonly type: string = LOAD_TRANSACTION;
	constructor(public payload: any = {}) {}
}

export class LoadSuccess implements Action {
	readonly type: string = LOAD_TRANSACTION_SUCCESS;
	constructor(public payload: any = {}) {}
}

export class LoadFail implements Action {
	readonly type: string = LOAD_TRANSACTION_FAIL;
	constructor(public payload: any = {}) {}
}

export class LoadAll implements Action {
	readonly type: string = LOAD_ALL_TRANSACTIONS;
	constructor(public payload: any = {}) {}
}

export class LoadAllSuccess implements Action {
	readonly type: string = LOAD_ALL_TRANSACTIONS_SUCCESS;
	constructor(public payload: any = {}) {}
}

export class LoadAllFail implements Action {
	readonly type: string = LOAD_ALL_TRANSACTIONS_FAIL;
	constructor(public payload: any = {}) {}
}

export class Save implements Action {
	readonly type: string = SAVE_TRANSACTION;
	constructor(public payload: any = {}) {}
}

export class SaveSuccess implements Action {
	readonly type: string = SAVE_TRANSACTION_SUCCESS;
	constructor(public payload: any = {}) {}
}

export class SaveFail implements Action {
	readonly type: string = SAVE_TRANSACTION_FAIL;
	constructor(public payload: any = {}) {}
}

export class Edit implements Action {
	readonly type: string = EDIT_TRANSACTION;
	constructor(public payload: any = {}) {}
}

export class EditSuccess implements Action {
	readonly type: string = EDIT_TRANSACTION_SUCCESS;
	constructor(public payload: any = {}) {}
}

export class EditFail implements Action {
	readonly type: string = EDIT_TRANSACTION_FAIL;
	constructor(public payload: any = {}) {}
}

export type Actions =
	Load |
	LoadSuccess |
	LoadFail |
	Save |
	SaveSuccess |
	SaveFail |
	Edit |
	EditSuccess |
	EditFail;
