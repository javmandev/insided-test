import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Category } from '../models/category';
import { State, getCategories } from '../reducers/reducer';

@Injectable()
export class CategoryDataResolver implements Resolve<Category> {

	private category;

 	constructor(
		private store: Store<State>
	) {}

  	resolve(route: ActivatedRouteSnapshot): Observable<Category> {

		this.filterCategories(route);

		return this.category[0];
	}

	filterCategories(route): void {
		this.store.select(getCategories)
			.take(1)
			.subscribe(categories =>
				this.category = categories.filter(category =>
					+category.id === +route.paramMap.get('id')
				)
			);
	}
}
