import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import { of } from 'rxjs/observable/of';

import { State, getCategories } from '../reducers/reducer';
import { Load } from '../actions/actions';
import { Category } from '../models/category';

@Injectable()
export class CategoriesLoadedGuard implements CanActivate {

  	constructor(
	  	private store: Store<State>
	) {}

  	getDataFromStore(): Observable<boolean> {
		return this.store
			.select(getCategories)
			.do((categories: any) => {
				if (!categories.length) {
					this.store.dispatch(new Load());
				}
			})
			.filter((categories: Category[]) => categories.length > 0)
			.take(1);
  	}

  	canActivate(): Observable<boolean> {
		return this.getDataFromStore()
			.switchMap(() => of(true))
			.catch(() => of(false));
  	}
}
