// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

// Containers / Components
import { SelectCategoryContainer } from './containers/select-category';
import { SelectCategoryComponent } from './components/select-category';
import { ViewCategoriesContainer } from './containers/view-categories';
import { ViewCategoriesComponent } from './components/view-categories';
import { CategoryFormComponent } from './components/category-form';
import { NewCategoryContainer } from './containers/new-category';
import { EditCategoryContainer } from './containers/edit-category';

// Effects
import { CategoryEffects } from './effects/effects';

// Providers
import { CategoryService } from './providers/category';
import { CategoriesLoadedGuard } from './guards/categories-loaded.guard';
import { CategoryDataResolver } from './guards/category-data.resolver';

// Reducers
import { reducer } from './reducers/reducer';

// Routes
import { routes } from './routes';

const COMPONENTS = [
	SelectCategoryContainer,
	SelectCategoryComponent,
	ViewCategoriesContainer,
	ViewCategoriesComponent,
	CategoryFormComponent,
	NewCategoryContainer,
	EditCategoryContainer
];

@NgModule({
	declarations: COMPONENTS,
	imports: [
		CommonModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		StoreModule.forFeature('categories', reducer),
		EffectsModule.forFeature([CategoryEffects]),
		RouterModule.forChild(routes)
	],
	providers: [
		CategoryService,
		CategoriesLoadedGuard,
		CategoryDataResolver
	],
	exports: COMPONENTS
})
export class CategoryModule {}
