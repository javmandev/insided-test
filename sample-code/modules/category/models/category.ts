export interface Category {
	id: number;
	parent: number;
	name: string;
	icon: string;
	subcategories: Category[];
}
