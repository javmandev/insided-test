import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';

import * as actions from '../actions/actions';
import { CategoryService } from '../providers/category';
import { Category } from '../models/category';

@Injectable()
export class CategoryEffects {
	constructor (
		private actions$: Actions,
		private categoryService: CategoryService,
		private router: Router
	) {}

	@Effect() loadCategories$: Observable<Action> = this.actions$
		.ofType(actions.LOAD_CATEGORIES)
		.map<Action, Category>(toPayload)
		.switchMap(() => this.categoryService.getAllCategories()
			.map(categories => {
				if (categories.length) {
					return new actions.LoadSuccess(categories);
				}
				return new actions.LoadFail({code: 999, message: 'There are no categories to load.'});
			})
			.catch(error => Observable.of(new actions.LoadFail(error)))
		);

	@Effect() addCategory$: Observable<Action> = this.actions$
		.ofType(actions.ADD_CATEGORY)
		.map<Action, Category>(toPayload)
		.switchMap(category => this.categoryService.addCategory(category)
			.mergeMap(() => {
				this.router.navigate(['categories']);
				return [
					new actions.AddSuccess(),
					new actions.Load(),
				];
			})
			.catch(error => Observable.of(new actions.AddFail(error)))
		);

	@Effect() editCategory$: Observable<Action> = this.actions$
		.ofType(actions.EDIT_CATEGORY)
		.map<Action, Category>(toPayload)
		.switchMap(category => this.categoryService.editCategory(category)
			.mergeMap(() =>
			{
				this.router.navigate(['categories']);
				return [
					new actions.EditSuccess(),
					new actions.Load(),
				];
			})
			.catch(error => Observable.of(new actions.EditFail(error)))
		);

	@Effect() deleteCategory$: Observable<Action> = this.actions$
		.ofType(actions.DELETE_CATEGORY)
		.map<Action, number>(toPayload)
		.switchMap(categoryId => this.categoryService.deleteCategory(categoryId)
			.mergeMap(() => {
				this.router.navigate(['categories']);
				return [
					new actions.DeleteSuccess(),
					new actions.Load(),
				];
			})
			.catch(error => Observable.of(new actions.DeleteFail(error)))
		);

}
