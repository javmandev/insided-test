import { Component, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { Category } from '../models/category';

@Component({
	selector: 'select-category-component',
	templateUrl: './select-category.html',
	styleUrls: ['./select-category.scss'],
	animations: [
		trigger('show', [
			state('true', style({ height: '*' })),
			state('false', style({ height: '0px' })),
			transition('* => *', animate('300ms ease-out'))
		])
	]
})
export class SelectCategoryComponent {

	public showSubcategories: boolean[] = [];

	@Input() categories: Category[];
	@Input() visible: boolean = false;
	@Input() selectedCategory: Category;

	@Output() onOpenModal = new EventEmitter;
	@Output() onCloseModal = new EventEmitter;
	@Output() onSelectCategory = new EventEmitter;

	openModal(): void {
		this.onOpenModal.emit();
	}

	closeModal(): void {
		this.onCloseModal.emit();
	}

	selectCategory(category: Category): void {
		this.onSelectCategory.emit(category);
	}

	toggleSubcategories(id: number): void {
		this.showSubcategories[id] = !this.showSubcategories[id];
	}

	trackCategories(index: number, category: Category) {
		return category ? category.id : undefined;
	}
}
