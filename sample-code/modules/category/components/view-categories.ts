import { Component, Input } from '@angular/core';

import { Category } from '../models/category';

@Component({
	selector: 'view-categories-component',
	templateUrl: './view-categories.html',
	styleUrls: ['./view-categories.scss']
})
export class ViewCategoriesComponent {

	@Input() categories: Category[];

	trackCategories(index: number, category: Category) {
		return category ? category.id : undefined;
	}
}
