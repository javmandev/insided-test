import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Category } from '../models/category';

@Component({
	selector: 'category-form-component',
	templateUrl: './category-form.html',
	styleUrls: ['./category-form.scss']
})
export class CategoryFormComponent {

	@Input() categories: Category[];
	@Input() form: FormGroup;

	@Output() onSubmitForm = new EventEmitter;
	@Output() onDelete = new EventEmitter;

	submitForm() {
		this.onSubmitForm.emit();
	}

	deleteCategory() {
		this.onDelete.emit();
	}
}
