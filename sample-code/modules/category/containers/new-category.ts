import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { State, getRootCategories } from '../reducers/reducer';
import { Add } from '../actions/actions';

@Component({
	selector: 'new-category',
	template: `<category-form-component
				[categories]="categories | async"
				[form]="categoryForm"
				(onSubmitForm)="handleSubmitForm()">
			</category-form-component>`
})
export class NewCategoryContainer {

	public categoryForm: FormGroup;

	constructor(
		private store: Store<State>,
		public fb: FormBuilder
	) {
		this.setupForm();
	}

	public get categories() {
		return this.store.select(getRootCategories);
	}

	setupForm() {
		this.categoryForm = this.fb.group({
			name: [ '', [ Validators.required ] ],
			icon: '',
			parent: [ 1, [ Validators.required ] ],
		});
	}

	handleSubmitForm() {
		this.store.dispatch(new Add(this.categoryForm.value));
	}
}
