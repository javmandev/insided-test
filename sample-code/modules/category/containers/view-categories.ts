import { Component } from '@angular/core';

import { CategoryService } from '../providers/category';

@Component({
	selector: 'view-categories',
	template: `<view-categories-component
				[categories]="categories">
			</view-categories-component>`
})
export class ViewCategoriesContainer {

	constructor(
		private categoryService: CategoryService
	) {}

	public get categories() {
		return this.categoryService.organisedCategories();
	}

}
