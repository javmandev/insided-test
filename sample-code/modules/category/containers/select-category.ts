import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { State, getSelectedCategory } from '../reducers/reducer';
import { Category } from '../models/category';
import { Select } from '../actions/actions';

import { CategoryService } from '../providers/category';

@Component({
	selector: 'select-category',
	template: `<select-category-component
				[categories]="categories"
				[selectedCategory]="selectedCategory | async"
				[visible]="visible"
				(onOpenModal)="handleOpenModal()"
				(onCloseModal)="handleCloseModal()"
				(onSelectCategory)="handleSelectCategory($event)">
			</select-category-component>`
})
export class SelectCategoryContainer {

	public visible: boolean = false;

	constructor(
		private store: Store<State>,
		private categoryService: CategoryService
	) {}

	public get selectedCategory() {
		return this.store.select(getSelectedCategory);
	}

	public get categories() {

		return this.categoryService.organisedCategories();
	}

	handleOpenModal() {
		this.visible = true;
	}

	handleCloseModal() {
		this.visible = false;
	}

	handleSelectCategory(e) {
		this.store.dispatch(new Select(e));
		this.handleCloseModal();
	}
}
