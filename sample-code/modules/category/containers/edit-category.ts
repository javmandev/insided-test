import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';

import { State, getRootCategories } from '../reducers/reducer';
import { Edit, Delete } from '../actions/actions';
import { Category } from '../models/category';

@Component({
	selector: 'edit-category',
	template: `<category-form-component
				[categories]="categories | async"
				[form]="categoryForm"
				(onSubmitForm)="handleSubmitForm()"
				(onDelete)="handleDelete()">
			</category-form-component>`
})
export class EditCategoryContainer {

	public categoryForm: FormGroup;
	public category: Category;

	constructor(
		private store: Store<State>,
		public fb: FormBuilder,
		private activatedRoute: ActivatedRoute
	) {
		this.category = this.activatedRoute.snapshot.data.category;
		this.setupForm();
	}

	public get categories() {
		return this.store.select(getRootCategories);
	}

	setupForm() {
		this.categoryForm = this.fb.group({
			id: [ this.category.id, [ Validators.required ] ],
			name: [ this.category.name, [ Validators.required ] ],
			icon: this.category.icon,
			parent: [ this.category.parent, [ Validators.required ] ],
		});
	}

	handleSubmitForm() {
		this.store.dispatch(new Edit(this.categoryForm.value));
	}

	handleDelete() {
		this.store.dispatch(new Delete(this.category.id));
	}
}
