import { ActionReducer, createFeatureSelector, createSelector } from '@ngrx/store';

import { BasicState } from '../../../shared/models/basic-state';
import { Category } from '../models/category';

import * as categoryActions from '../actions/actions';

export interface State extends BasicState {
	categories: Category[];
	selectedCategory: Category;
	defaultCategory: Category;
}

export const INITIAL_STATE: State = {
	categories: [],
	selectedCategory: undefined,
	defaultCategory: undefined,
	isLoading: false,
	hasErrors: false,
	errors: []
};

export function reducer(state = INITIAL_STATE, action: categoryActions.Actions): State {

	switch (action.type) {

		case categoryActions.ADD_CATEGORY:
		case categoryActions.EDIT_CATEGORY:
		case categoryActions.DELETE_CATEGORY:
		case categoryActions.LOAD_CATEGORIES:
			return handleAction(state);

		case categoryActions.LOAD_CATEGORIES_SUCCESS:
			return handleLoadSuccess(state, action);

		case categoryActions.ADD_CATEGORY_SUCCESS:
		case categoryActions.EDIT_CATEGORY_SUCCESS:
		case categoryActions.DELETE_CATEGORY_SUCCESS:
			return handleSuccess(state, action);

		case categoryActions.ADD_CATEGORY_FAIL:
		case categoryActions.EDIT_CATEGORY_FAIL:
		case categoryActions.DELETE_CATEGORY_FAIL:
		case categoryActions.LOAD_CATEGORIES_FAIL:
			return handleFail(state, action);

		case categoryActions.SELECT_CATEGORY:
			return handleSelectAction(state, action);

		case categoryActions.SELECT_DEFAULT_CATEGORY:
			return handleSelectDefaultAction(state);

		case categoryActions.SET_DEFAULT_CATEGORY:
			return handleSetDefaultAction(state, action);

		default:
			return state;

	}
}

function handleAction(state: State): State {
	return {
		...state,
		isLoading: true
	};
}

function handleLoadSuccess(state: State, action: categoryActions.Actions): State {

	let defCat = action.payload.filter(c => c.id === 1);

	return {
		...state,
		categories: [...action.payload],
		defaultCategory: {...defCat[0]},
		selectedCategory: {...defCat[0]},
		isLoading: false,
		hasErrors: false
	};
}

function handleSuccess(state: State, action: categoryActions.Actions): State {
	return {
		...state,
		isLoading: false,
		hasErrors: false
	};
}

function handleFail(state: State, action: categoryActions.Actions): State {
	return {
		...state,
		errors: [ ...state.errors, action.payload ],
		isLoading: false,
		hasErrors: true
	};
}

function handleSelectAction(state: State, action: categoryActions.Actions): State {
	return {
		...state,
		selectedCategory: {...action.payload}
	};
}

function handleSelectDefaultAction(state: State): State {
	return {
		...state,
		selectedCategory: {...state.defaultCategory}
	};
}

function handleSetDefaultAction(state: State, action: categoryActions.Actions): State {

	let defCat = action.payload.filter(c => c.id === 1);

	return {
		...state,
		defaultCategory: {...defCat}
	};
}

export const getState = createFeatureSelector<State>('categories');

export const getCategories = createSelector(
	getState,
	state => state.categories
);

export const getRootCategories = createSelector(
	getState,
	state => state.categories.filter(c => c.parent === 1)
);

export const getSelectedCategory = createSelector(
	getState,
	state => state.selectedCategory
);

export const getDefaultCategory = createSelector(
	getState,
	state => state.defaultCategory
);
