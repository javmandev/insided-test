import { Action } from '@ngrx/store';

export const LOAD_CATEGORIES = '[Category] Load';
export const LOAD_CATEGORIES_SUCCESS = '[Category] Load Success';
export const LOAD_CATEGORIES_FAIL = '[Category] Load Fail';

export const ADD_CATEGORY = '[Category] Add';
export const ADD_CATEGORY_SUCCESS = '[Category] Add Success';
export const ADD_CATEGORY_FAIL = '[Category] Add Fail';

export const EDIT_CATEGORY = '[Category] Edit';
export const EDIT_CATEGORY_SUCCESS = '[Category] Edit Success';
export const EDIT_CATEGORY_FAIL = '[Category] Edit Fail';

export const DELETE_CATEGORY = '[Category] Delete';
export const DELETE_CATEGORY_SUCCESS = '[Category] Delete Success';
export const DELETE_CATEGORY_FAIL = '[Category] Delete Fail';

export const SELECT_CATEGORY = '[Category] Select';

export const SET_DEFAULT_CATEGORY = '[Category] Set Default';
export const SELECT_DEFAULT_CATEGORY = '[Category] Select Default';

export class Load implements Action {
	readonly type: string = LOAD_CATEGORIES;
	constructor(public payload: any = {}) {}
}

export class LoadSuccess implements Action {
	readonly type: string = LOAD_CATEGORIES_SUCCESS;
	constructor(public payload: any = {}) {}
}

export class LoadFail implements Action {
	readonly type: string = LOAD_CATEGORIES_FAIL;
	constructor(public payload: any = {}) {}
}

export class Add implements Action {
	readonly type: string = ADD_CATEGORY;
	constructor(public payload: any = {}) {}
}

export class AddSuccess implements Action {
	readonly type: string = ADD_CATEGORY_SUCCESS;
	constructor(public payload: any = {}) {}
}

export class AddFail implements Action {
	readonly type: string = ADD_CATEGORY_FAIL;
	constructor(public payload: any = {}) {}
}

export class Edit implements Action {
	readonly type: string = EDIT_CATEGORY;
	constructor(public payload: any = {}) {}
}

export class EditSuccess implements Action {
	readonly type: string = EDIT_CATEGORY_SUCCESS;
	constructor(public payload: any = {}) {}
}

export class EditFail implements Action {
	readonly type: string = EDIT_CATEGORY_FAIL;
	constructor(public payload: any = {}) {}
}

export class Delete implements Action {
	readonly type: string = DELETE_CATEGORY;
	constructor(public payload: any = {}) {}
}

export class DeleteSuccess implements Action {
	readonly type: string = DELETE_CATEGORY_SUCCESS;
	constructor(public payload: any = {}) {}
}

export class DeleteFail implements Action {
	readonly type: string = DELETE_CATEGORY_FAIL;
	constructor(public payload: any = {}) {}
}

export class Select implements Action {
	readonly type: string = SELECT_CATEGORY;
	constructor(public payload: any = {}) {}
}

export class SelectDefault implements Action {
	readonly type: string = SELECT_DEFAULT_CATEGORY;
	constructor(public payload: any = {}) {}
}

export class SetDefault implements Action {
	readonly type: string = SET_DEFAULT_CATEGORY;
	constructor(public payload: any = {}) {}
}

export type Actions =
	Load |
	LoadSuccess |
	LoadFail |
	Add |
	AddSuccess |
	AddFail |
	Edit |
	EditSuccess |
	EditFail |
	Delete |
	DeleteSuccess |
	DeleteFail |
	Select |
	SetDefault |
	SelectDefault;
