import { TestBed } from '@angular/core/testing';
import { CategoryService } from './category';
import { CategoryModule } from '../module';
import { Store } from '@ngrx/store';

describe('Category Service', () => {
	let service: CategoryService;
	let store;

	const mockCategories = [
		{ id: 1, parent: 1, icon: 'default', name: 'Default category', subcategories: [] },
		{ id: 2, parent: 1, icon: 'default', name: 'Cat 2', subcategories: [] },
		{ id: 3, parent: 1, icon: 'default', name: 'Cat 3', subcategories: [] },
		{ id: 4, parent: 3, icon: 'default', name: 'Cat 4', subcategories: [] },
		{ id: 5, parent: 3, icon: 'default', name: 'Cat 5', subcategories: [] },
	];

	const mockOrganisedResult = [
		{ id: 1, parent: 1, icon: 'default', name: 'Default category', subcategories: [
			{ id: 2, parent: 1, icon: 'default', name: 'Cat 2', subcategories: [] },
			{ id: 3, parent: 1, icon: 'default', name: 'Cat 3', subcategories: [] }
		] },
		{ id: 2, parent: 1, icon: 'default', name: 'Cat 2', subcategories: [] },
		{ id: 3, parent: 1, icon: 'default', name: 'Cat 3', subcategories: [
			{ id: 4, parent: 3, icon: 'default', name: 'Cat 4', subcategories: [] },
			{ id: 5, parent: 3, icon: 'default', name: 'Cat 5', subcategories: [] }
		] }
	];

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [ CategoryModule ]
		});

		/* store = TestBed.get(Store); */

		/* spyOn(store, 'select').and.returnValue(mockCategories);

		service = TestBed.get(CategoryService); */
	});
	it('should return root categories with populated subcategories when organisedCategories is called', () => {
		/* const organised = service.organisedCategories();
		expect(organised).toEqual(mockOrganisedResult); */
	});
});
