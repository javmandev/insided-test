import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Category } from '../models/category';
import { State, getCategories } from '../reducers/reducer';

@Injectable()
export class CategoryService {

	private categories: Category[];
	private api: string = 'http://localhost:3000/categories';

	constructor(
		private http: HttpClient,
		private store: Store<State>
	) {
		this.store.select(getCategories).subscribe(categories => this.categories = categories);
	}

	getAllCategories(): Observable<any> {

		return this.http.get(`${this.api}/`);
	}

	addCategory(category: Category): Observable<any> {

		return this.http.post(`${this.api}/new`, category);
	}

	editCategory(category: Category): Observable<any> {

		return this.http.put(`${this.api}/edit`, category);
	}

	deleteCategory(categoryId: number): Observable<any> {

		return this.http.delete(`${this.api}/${categoryId}`);
	}

	organisedCategories() {

		let organised = [];

		let rootCategories = [ ...this.categories.filter(category => category.parent === 1) ];

		rootCategories.map(category => {

			if ( category.id === 1 ) {
				organised.push({
					...category
				});
			} else {
				organised.push({
					...category,
					subcategories: this.categories.filter(cat => cat.parent === category.id)
				});
			}

		});

		return organised;
	}

	findCategoryById(id) {
		return this.categories.filter(category => category.id === id)[0];
	}
}
