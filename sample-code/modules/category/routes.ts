import { Routes } from '@angular/router';

import { ViewCategoriesContainer } from './containers/view-categories';
import { NewCategoryContainer } from './containers/new-category';
import { EditCategoryContainer } from './containers/edit-category';

import { CategoriesLoadedGuard } from './guards/categories-loaded.guard';
import { CategoryDataResolver } from './guards/category-data.resolver';

export const routes: Routes = [
	{
		path: 'categories',
		canActivate: [ CategoriesLoadedGuard ],
		children: [
			{
				path: '',
				pathMatch: 'full',
				component: ViewCategoriesContainer
			},
			{
				path: 'new',
				component: NewCategoryContainer
			},
			{
				path: 'edit',
				children: [
					{
						path: '',
						pathMatch: 'full',
						redirectTo: '/categories'
					},
					{
						path: ':id',
						component: EditCategoryContainer,
						resolve: { category: CategoryDataResolver }
					}
				]
			}
		]
	}
];
