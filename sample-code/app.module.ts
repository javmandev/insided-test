// Modules
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RouterModule } from '@angular/router';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClientModule } from '@angular/common/http';

import { SharedModule } from './modules/shared/module';
import { TransactionModule } from './modules/transaction/module';
import { AccountModule } from './modules/account/module';
import { CategoryModule } from './modules/category/module';

// Core
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';

// Containers / Components
import { AppComponent } from './app.component';

// Reducers
import { reducers, metaReducers } from './reducers/reducers';

// Utils
import { CustomRouterStateSerializer } from './shared/utils/custom-router-state-serializer';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		SharedModule,
		TransactionModule,
		AccountModule,
		CategoryModule,
		StoreModule.forRoot(reducers, { metaReducers }),
		EffectsModule.forRoot([]),
		RouterModule.forRoot([]),
		StoreRouterConnectingModule,
		StoreDevtoolsModule.instrument()
	],
	providers: [
		{ provide: RouterStateSerializer, useClass: CustomRouterStateSerializer }
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
