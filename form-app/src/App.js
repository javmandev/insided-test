import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import From from './components/form';
import axios from 'axios';

class App extends Component {

	state = {
		status: '',
		type: ''
	}

	makeid() {
		const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		let text = '';
	  
		for (let i = 0; i < 18; i++)
		  text += possible.charAt(Math.floor(Math.random() * possible.length));
	  
		return text;
	}

  	postSubmit = (e) => {
    	e.preventDefault();
		
		const data = {
			userId: this.makeid(),
			message: e.target.comment.value
		}

		axios.post('http://insided.javiernarvaez.com/api/posts/create.php', data).then(() => {
			this.setState({ status: 'Comment submitted successfully', type: 'success' });
			document.getElementById("create-post-form").reset();
		}).catch(e => {
			this.setState({ status: 'Comment submission failed', type: 'error' });
		});
	}
	  
	dismissStatus = () => {
		this.setState({ status: '', type: '' });
	}

	render() {
		const statusMessage = this.state.status !== '' ? (
			<div className={'form-status ' + this.state.type}>
				{this.state.status}
				<div>
					<button className="dismiss" onClick={() => this.dismissStatus()}>Dismiss</button>
				</div>
			</div>
		) : null;

		return (
		<div className="App">
			<header className="App-header">
				<h1 className="App-title">Create your post</h1>
			</header>
			<div className="App-intro">
				<From submit={() => this.postSubmit} />
			</div>
			{statusMessage}
		</div>
		);
	}
}

export default App;
