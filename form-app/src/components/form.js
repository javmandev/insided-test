import React from 'react';

const Form = (props) => {
    return (
        <form onSubmit={props.submit(this)} id="create-post-form">
            <textarea name="comment" placeholder="Add a comment" />
            <div>
                <button type="submit">Submit</button>
            </div>
        </form>
    )
}

export default Form;