<?php
// required headers for accessing the server from a different port/domain
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/core.php';
include_once '../config/database.php';
include_once '../objects/posts.php';
include_once '../shared/utils.php';

// instantiate database and product object
$utils = new Utils();
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$insidedPost = new InsidedPost($db);

// query products
$stmt = $insidedPost->read($from_record_num, $records_per_page);
$num = $stmt->rowCount();

// check if more than 0 record found
if ( $num > 0 ) {

    $posts_arr = array();
    $posts_arr["data"] = array();
    $posts_arr["paging"] = array();
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);
 
        $single_post = array(
            "id" => $id,
            "userId" => $userId,
            "message" => html_entity_decode($message),
            "timestamp" => $timestamp
        );
 
        array_push($posts_arr["data"], $single_post);
    }

    $total_rows = $insidedPost->count();
    $page_url = "{$home_url}posts/read.php?";
    $paging = $utils->getPaging($page, $total_rows, $records_per_page, $page_url);
    $posts_arr["paging"] = $paging;

    echo json_encode($posts_arr);
}
else
{
    echo json_encode(
        array("message" => "No posts found.")
    );
}
?>