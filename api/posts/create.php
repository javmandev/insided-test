<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../objects/posts.php';
 
$database = new Database();
$db = $database->getConnection();
$insidedPost = new InsidedPost($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// set post property values
$insidedPost->userId = $data->userId;
$insidedPost->message = $data->message;
 
// create the post
if ($insidedPost->userId !== "" && $insidedPost->message !== "") {
    if($insidedPost->create()){
        echo '{';
            echo '"message": "Post was created."';
        echo '}';
    }
    else
    {
        http_response_code(500);
        echo '{';
            echo '"message": "Unable to create post."';
        echo '}';
    }
}
else
{
    http_response_code(400);
    echo '{';
        echo '"message": "Missing data"';
    echo '}';
}
?>