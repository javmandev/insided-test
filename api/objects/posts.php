<?php
class InsidedPost {

    private $conn;
    private $table_name = "posts";
 
    // object properties
    public $id;
    public $userId;
    public $message;
    public $timestamp;
 
    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    public function count(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
    
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        return $row['total_rows'];
    }

    function read($from_record_num, $records_per_page) {
 
        // select all query
        $query = "SELECT * FROM " . $this->table_name . " ORDER BY timestamp DESC LIMIT " . $from_record_num . ", " . $records_per_page;
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    function create() {
    
        // query to insert record
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    userId=:userId, message=:message";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->userId = htmlspecialchars(strip_tags($this->userId));
        $this->message = htmlspecialchars(strip_tags($this->message));
    
        // bind values
        $stmt->bindParam(":userId", $this->userId);
        $stmt->bindParam(":message", $this->message);
    
        // execute query
        if ( $stmt->execute() ) {
            return true;
        }
    
        return false;
    }
}
?>