<?php
class Database {

    private $host = "db738300024.db.1and1.com";
    private $db_name = "db738300024";
    private $username = "dbo738300024";
    private $password = "thisIS!nsid3d";
    public $conn;

    public function getConnection() {
 
        $this->conn = null;
 
        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }
        catch( PDOException $exception ) {
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}
?>