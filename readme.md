# Online urls

Form:
- http://insided.javiernarvaez.com/index.html

Widget:
- http://insided.javiernarvaez.com/widget/index.html

API:
- http://insided.javiernarvaez.com/api/posts/read.php
- http://insided.javiernarvaez.com/api/posts/create.php

# Questionaire

https://docs.google.com/document/d/1bXKD3I-lPIv1CX9v1cJzDts0OPuKq-qwEYGw72bgbgg/edit?usp=sharing

# Sample code

Within this repo, sample-code folder.